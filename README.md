# Prometheus & Grafana

[Prometheus](https://prometheus.io/) is the defacto standard for collecting metrics. Sensorita exposes a metrics endpoint (`/metrics`) which sensor data such as Temperature, Humidity). [Grafana](https://grafana.com/) is a analytics and monitoring system which creates beautiful charts of your data. 

## Prometheus

See the `prometheus.yml` file in this repo and change it to your needs. You will need to chagne the targets (you might just have one). A target is the place Prometheus tries to find a `/metrics` endpoint for collecting all the metrics that are exposed. 

```
docker run -d -p 9090:9090 -v /home/pi/promdata:/prometheus -v /home/pi/grafana/prometheus.yml:/etc/prometheus/prometheus.yml --restart always --name prometheus rycus86/prometheus
```

## Grafana

To run Grafana, use the docker command below. Note: chjange the admin password and server domain first. If you do not have a server domain/root just remove both values. 

Creating a named volume ensures that your dashboard data is not lost between restarts. 

```
docker volume create grafana-storage
```

```
docker run \
  -d \
  -p 3000:3000 \
  --name=grafana \
  --restart always \
  -v grafana-storage:/var/lib/grafana \
  -e "GF_SERVER_ROOT_URL=https://grafana.example.com" \
  -e "GF_SERVER_DOMAIN=grafana.example.de" \
  -e "GF_SECURITY_ADMIN_PASSWORD=<ADMIN_PW_HERE>" \
  rycus86/grafana
```

